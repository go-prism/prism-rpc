# Prism RPC

This project contains the Protobuf definitions used by the Prism project.
Proto's generated here are considered the source-of-truth and are used for gRPC, REST and SQL operations.

## Getting started

1. Install `protoc`
1. Run `make install`
1. Run `make generate` to generate GoLang code
1. Run `make generate-ts` to generate TypeScript code

## Adding `proto` files

1. Create your `.proto` file
2. Edit the `Makefile` to include the new file
3. Run `make generate`
