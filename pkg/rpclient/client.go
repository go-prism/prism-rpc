package rpclient

import (
	"crypto/tls"
	"crypto/x509"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// NewAutoConn attempts to figure out which connection type
// is requested based on the given files.
//
// cert + key + ca -> mTLS
//
// ca -> TLS
//
// nothing -> plain text
func NewAutoConn(addr, cert, key, ca string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	if cert == "" || key == "" {
		if ca == "" {
			log.Infof("missing CA, creating plain text connection to %s", addr)
			return NewBaseConn(addr, opts...)
		}
		log.Infof("missing cert or key, creating TLS connection to %s", addr)
		return NewTLSConn(addr, ca, opts...)
	}
	log.Infof("creating mTLS connection to %s", addr)
	return NewMutualTLSConn(addr, cert, key, ca, opts...)
}

// NewBaseConn establishes an unencrypted connection
// with a given service.
func NewBaseConn(addr string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(addr, append(opts, grpc.WithInsecure())...)
	if err != nil {
		log.WithError(err).Errorf("failed to connect to service: %s", addr)
		return nil, err
	}
	return conn, nil
}

// NewTLSConn establishes an encrypted connection
// with a given service and verifies that it is using
// a server cert from the given CA.
func NewTLSConn(addr, ca string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	log.Infof("reading CA from file: %s", ca)
	creds, err := credentials.NewClientTLSFromFile(ca, "")
	if err != nil {
		log.WithError(err).Error("failed to setup client TLS")
		return nil, err
	}
	conn, err := grpc.Dial(addr, append(opts, grpc.WithTransportCredentials(creds))...)
	if err != nil {
		log.WithError(err).Errorf("failed to connect to service: %s", addr)
		return nil, err
	}
	return conn, nil
}

// NewMutualTLSConn establishes an encrypted connection
// with a given services using MutualTLS.
func NewMutualTLSConn(addr, cert, key, ca string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	// try to read the system CA certs, or create a new pool
	caRoot, err := x509.SystemCertPool()
	if err != nil {
		log.WithError(err).Error("failed to read system certificate pool, creating a new one")
		caRoot = x509.NewCertPool()
	} else {
		log.Debug("successfully read x509 system certificate pool")
	}
	var certs []tls.Certificate
	keyPair, caData, err := utils.ReadTriplet(cert, key, ca)
	if err != nil {
		return nil, err
	}
	certs = append(certs, *keyPair)
	// try to append the CA
	if !caRoot.AppendCertsFromPEM(caData) {
		log.Error("failed to append given CA cert")
	} else {
		log.Info("successfully appended CA cert to system roots")
	}

	creds := credentials.NewTLS(&tls.Config{
		ClientCAs:    caRoot,
		Certificates: certs,
		MinVersion:   tls.VersionTLS13,
	})
	conn, err := grpc.Dial(addr, append(opts, grpc.WithTransportCredentials(creds))...)
	if err != nil {
		log.WithError(err).Errorf("failed to connect to service: %s", addr)
		return nil, err
	}
	return conn, nil
}
