package utils

import (
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

// GetOpts returns a []grpc.ServerOption containing
// a number of preset server configuration.
//
// 1. logrus logging integration
// 2. (not yet) cap10 auth integration
func GetOpts(streamOpts []grpc.StreamServerInterceptor, unaryOpts []grpc.UnaryServerInterceptor) []grpc.ServerOption {
	entry := log.NewEntry(log.StandardLogger())
	grpc_logrus.ReplaceGrpcLogger(entry)

	return []grpc.ServerOption{
		grpc_middleware.WithStreamServerChain(
			append([]grpc.StreamServerInterceptor{
				grpc_ctxtags.StreamServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
				grpc_logrus.StreamServerInterceptor(entry),
				grpc_auth.StreamServerInterceptor(tracerFunc),
				grpc_prometheus.StreamServerInterceptor,
			}, streamOpts...)...,
		),
		grpc_middleware.WithUnaryServerChain(
			append([]grpc.UnaryServerInterceptor{
				grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
				grpc_logrus.UnaryServerInterceptor(entry),
				grpc_auth.UnaryServerInterceptor(tracerFunc),
				grpc_prometheus.UnaryServerInterceptor,
			}, unaryOpts...)...,
		),
	}
}
