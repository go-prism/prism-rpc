package utils

import (
	"crypto/tls"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
)

// ReadTriplet parses a x509 keypair and
// CA from the given files.
func ReadTriplet(cert, key, ca string) (*tls.Certificate, []byte, error) {
	// read the CA
	log.Infof("reading CA from file: %s", ca)
	caData, err := ioutil.ReadFile(ca)
	if err != nil {
		log.WithError(err).Error("failed to read CA file")
		return nil, nil, err
	}
	// read the Certificate keypair
	keyPair, err := tls.LoadX509KeyPair(cert, key)
	if err != nil {
		log.WithError(err).Error("failed to x509 read keypair")
		return nil, nil, err
	}
	log.Info("successfully read x509 keypair")
	return &keyPair, caData, nil
}
