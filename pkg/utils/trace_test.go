package utils

import (
	"context"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/metadata"
	"testing"
)

var tracerFuncTests = []struct {
	name string
	in   context.Context
	out  string
}{
	{
		"Missing metadata returns no ID",
		metadata.NewIncomingContext(context.TODO(), nil),
		"",
	},
	{
		"Missing metadata key returns no ID",
		metadata.NewIncomingContext(context.TODO(), metadata.New(map[string]string{})),
		"",
	},
	{
		"Metadata key returns correct ID",
		metadata.NewIncomingContext(context.TODO(), metadata.New(map[string]string{
			tracer.DefaultRequestHeader: "1234",
		})),
		"1234",
	},
}

func TestTracerFunc(t *testing.T) {
	for _, tt := range tracerFuncTests {
		t.Run(tt.name, func(t *testing.T) {
			newCtx, err := tracerFunc(tt.in)
			assert.NoError(t, err)

			id := tracer.GetContextId(newCtx)
			assert.Equal(t, tt.out, id)
		})
	}
}
