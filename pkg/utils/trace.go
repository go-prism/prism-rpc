package utils

import (
	"context"
	"github.com/djcass44/go-tracer/tracer"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/metadata"
)

// tracerFunc extracts the RequestID from the incoming metadata
// and applies it in a form that can be read by the go-tracer package.
func tracerFunc(ctx context.Context) (context.Context, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		log.Debugf("cannot extract request ID from nil metadata")
		return ctx, nil
	}
	requestIDs := md.Get(tracer.DefaultRequestHeader)
	if len(requestIDs) < 1 {
		log.Debugf("failed to extract request ID from metadata key (%s)", tracer.DefaultRequestHeader)
		return ctx, nil
	}
	return context.WithValue(ctx, tracer.ContextKeyID, requestIDs[0]), nil
}

// SubmitCtx creates a new context with outgoing metadata
// to be sent over the gRPC wire
func SubmitCtx(ctx context.Context) context.Context {
	id := tracer.GetContextId(ctx)
	md := metadata.New(map[string]string{
		tracer.DefaultRequestHeader: id,
	})
	return metadata.NewOutgoingContext(ctx, md)
}
