package rpcserv_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-rpc/pkg/rpcserv"
	"google.golang.org/grpc"
	"net/http"
	"testing"
	"time"
)

func TestNewDualPurpose(t *testing.T) {
	srv, err := rpcserv.NewDualPurpose(0, &grpc.Server{}, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))
	assert.NoError(t, err)
	go func() {
		_ = srv.ListenAndServe()
	}()

	time.Sleep(time.Second)

	resp, err := http.Get(fmt.Sprintf("http://%s", srv.GetURL()))
	assert.NoError(t, err)
	defer resp.Body.Close()

	assert.Equal(t, http.StatusOK, resp.StatusCode)
}
