package rpcserv

import (
	"errors"
)

var (
	// ErrInvalidCA is thrown when a certificate was not successfully
	// added to the x509 CA pool
	ErrInvalidCA = errors.New("failed to append certificate to CA pool, it may not be a valid PEM file")
)

const (
	ApplicationGRPC = "application/grpc"
	ContentType     = "Content-Type"
)
