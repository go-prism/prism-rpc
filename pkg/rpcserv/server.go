package rpcserv

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
)

// DualPurpose is an http.Server capable of serving
// both H2C and gRPC content.
type DualPurpose struct {
	lis     net.Listener
	srv     *http.Server
	srvFunc http.Handler
}

// NewDualPurpose creates a new instance of DualPurpose
//goland:noinspection GoLinterLocal
func NewDualPurpose(port int, g *grpc.Server, h http.Handler) (*DualPurpose, error) {
	srv := new(DualPurpose)
	lis, err := srv.newListener(port)
	if err != nil {
		return nil, err
	}
	srv.lis = lis
	srv.srvFunc = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.Contains(r.Header.Get(ContentType), ApplicationGRPC) {
			g.ServeHTTP(w, r)
		} else {
			h.ServeHTTP(w, r)
		}
	})
	// initialise prometheus metrics
	grpc_prometheus.Register(g)
	return srv, nil
}

// newH2CServer creates the http.Handler required for the
// gRPC/HTTP negotiation to work over plain-text
func (srv *DualPurpose) newH2CServer() http.Handler {
	return h2c.NewHandler(srv.srvFunc, &http2.Server{})
}

// newH2Server creates the http.Handler required for the
// gRPC/HTTP negotiation to work over a TLS encrypted
// connection
func (srv *DualPurpose) newH2Server() http.Handler {
	return srv.srvFunc
}

func (srv *DualPurpose) newListener(port int) (net.Listener, error) {
	addr := fmt.Sprintf(":%d", port)
	log.Infof("listening on interface %s", addr)
	listener, err := net.Listen("tcp", addr)
	return listener, err
}

// ListenAndServeAuto attempts to figure out which connection type
// is requested based on the given files.
//
// cert + key + ca -> mTLS
//
// ca -> TLS
//
// nothing -> plain text
func (srv *DualPurpose) ListenAndServeAuto(cert, key, ca string) error {
	if cert == "" || key == "" {
		log.Info("cert or key is missing, starting H2C server")
		return srv.ListenAndServe()
	}
	if ca == "" {
		log.Info("CA is missing, starting H2 + TLS server")
		return srv.ListenAndServeTLS(cert, key)
	}
	log.Info("starting H2 + mTLS server")
	return srv.ListenAndServeMutualTLS(cert, key, ca)
}

// ListenAndServe starts the server using H2C
func (srv *DualPurpose) ListenAndServe() error {
	srv.srv = &http.Server{
		Handler: srv.newH2CServer(),
	}
	return srv.srv.Serve(srv.lis)
}

// ListenAndServeTLS starts the server using encrypted HTTP/2
func (srv *DualPurpose) ListenAndServeTLS(certFile, keyFile string) error {
	srv.srv = &http.Server{
		Handler: srv.newH2Server(),
		TLSConfig: &tls.Config{
			// https://ssl-config.mozilla.org/#server=go&version=1.14.4&config=modern&guideline=5.6
			MinVersion:               tls.VersionTLS13,
			PreferServerCipherSuites: true,
		},
	}
	return srv.srv.ServeTLS(srv.lis, certFile, keyFile)
}

// ListenAndServeMutualTLS starts the server using encrypted HTTP/2
// and requires that the client present a valid x509 client certificate
func (srv *DualPurpose) ListenAndServeMutualTLS(certFile, keyFile, caFile string) error {
	// try to read the system CA pool
	caPool, err := x509.SystemCertPool()
	if err != nil {
		log.WithError(err).Error("failed to read system CA pool, generating a new one")
		caPool = x509.NewCertPool()
	}
	// read the CA file
	log.Infof("attempting to read CA file: %s", caFile)
	data, err := ioutil.ReadFile(caFile)
	if err != nil {
		log.WithError(err).Error("failed to read CA file")
	}
	// try to append our cert to the pool
	if ok := caPool.AppendCertsFromPEM(data); !ok {
		log.Error("failed to append CA certificate to cert pool")
		return ErrInvalidCA
	}
	// start the server
	srv.srv = &http.Server{
		Handler: srv.newH2Server(),
		TLSConfig: &tls.Config{
			// https://ssl-config.mozilla.org/#server=go&version=1.14.4&config=modern&guideline=5.6
			MinVersion:               tls.VersionTLS13,
			PreferServerCipherSuites: true,
			ClientAuth:               tls.RequireAndVerifyClientCert,
			ClientCAs:                caPool,
		},
	}
	return srv.srv.ServeTLS(srv.lis, certFile, keyFile)
}

func (srv *DualPurpose) GetURL() string {
	return srv.lis.Addr().String()
}
