.PHONY: generate
generate:
	 protoc \
	 	-I. \
	 	-I${GOPATH}/pkg/mod \
	 	--go_out=. --go_opt=paths=source_relative \
         --go-grpc_out=. --go-grpc_opt=paths=source_relative \
         --gorm_out="engine=postgres:." \
         domain/v1/doc.proto \
         domain/v1/remote.proto \
         domain/v1/refract.proto \
         domain/v1/cache.proto \
         domain/v1/cache_svc.proto \
         domain/v1/reactor.proto \
         domain/v1/remote_status.proto \
         domain/v1/client.proto \
         domain/v1/roles.proto
	 protoc \
     		-I. \
     		-I${GOPATH}/pkg/mod \
     		--go_out=. --go_opt=paths=source_relative \
             --go-grpc_out=. --go-grpc_opt=paths=source_relative \
             --gorm_out="engine=postgres:." \
             domain/archv1/maven.proto \
			 domain/archv1/golang.proto
	protoc \
    		-I. \
    		-I${GOPATH}/pkg/mod \
    		--go_out=. --go_opt=paths=source_relative \
            --go-grpc_out=. --go-grpc_opt=paths=source_relative \
            service/reactor/api.proto \
            service/api/api.proto \
            service/api/remote.proto \
            service/api/refract.proto \
			service/api/golang.proto
	# https://stackoverflow.com/a/37335452
	# removes the 'omitempty' json tag
	ls domain/v1/*.pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'
	ls domain/archv1/*.pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'
	ls service/reactor/*.pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'
	ls service/api/*.pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'
	# fix the domain/v1 package name being imported relatively
	ls service/api/*.go | xargs -n1 -IX bash -c 'sed s#domain/v1#gitlab.com/go-prism/prism-rpc/domain/v1# X > X.tmp && mv X{.tmp,}'
	ls service/api/*.go | xargs -n1 -IX bash -c 'sed s#domain/archv1#gitlab.com/go-prism/prism-rpc/domain/archv1# X > X.tmp && mv X{.tmp,}'

.PHONY: generate-ts
generate-ts:
	mkdir -p build/gen
	protoc \
		-I. \
		-I${GOPATH}/pkg/mod \
		--plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts \
		--js_out=import_style=commonjs,binary:build/gen \
		--ts_out=build/gen \
		domain/v1/remote.proto \
		domain/v1/refract.proto \
		domain/v1/cache.proto \
		domain/v1/remote_status.proto \
		domain/v1/client.proto \
		domain/v1/roles.proto \
		domain/v1/reactor.proto \
		domain/archv1/maven.proto \
		domain/archv1/golang.proto \
		service/reactor/api.proto \
		service/api/remote.proto \
		service/api/refract.proto

install:
	go install \
		github.com/infobloxopen/protoc-gen-gorm@latest