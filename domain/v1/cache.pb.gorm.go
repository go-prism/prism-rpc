// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: domain/v1/cache.proto

package v1

import context "context"
import fmt "fmt"
import time "time"

import errors1 "github.com/infobloxopen/protoc-gen-gorm/errors"
import field_mask1 "google.golang.org/genproto/protobuf/field_mask"
import gorm1 "github.com/jinzhu/gorm"
import gorm2 "github.com/infobloxopen/atlas-app-toolkit/gorm"
import ptypes1 "github.com/golang/protobuf/ptypes"

import math "math"
import _ "google.golang.org/protobuf/types/known/timestamppb"

// Reference imports to suppress errors if they are not otherwise used.
var _ = fmt.Errorf
var _ = math.Inf

type CacheEntryORM struct {
	CreatedAt     *time.Time
	DownloadCount int64
	Id            string `gorm:"type:uuid;primary_key"`
	RefractID     string `gorm:"type:uuid"`
	RemoteID      string `gorm:"type:uuid"`
	UpdatedAt     *time.Time
	Uri           string
}

// TableName overrides the default tablename generated by GORM
func (CacheEntryORM) TableName() string {
	return "cache_entries"
}

// ToORM runs the BeforeToORM hook if present, converts the fields of this
// object to ORM format, runs the AfterToORM hook, then returns the ORM object
func (m *CacheEntry) ToORM(ctx context.Context) (CacheEntryORM, error) {
	to := CacheEntryORM{}
	var err error
	if prehook, ok := interface{}(m).(CacheEntryWithBeforeToORM); ok {
		if err = prehook.BeforeToORM(ctx, &to); err != nil {
			return to, err
		}
	}
	to.Id = m.Id
	if m.CreatedAt != nil {
		var t time.Time
		if t, err = ptypes1.Timestamp(m.CreatedAt); err != nil {
			return to, err
		}
		to.CreatedAt = &t
	}
	if m.UpdatedAt != nil {
		var t time.Time
		if t, err = ptypes1.Timestamp(m.UpdatedAt); err != nil {
			return to, err
		}
		to.UpdatedAt = &t
	}
	to.Uri = m.Uri
	to.DownloadCount = m.DownloadCount
	to.RemoteID = m.RemoteID
	to.RefractID = m.RefractID
	if posthook, ok := interface{}(m).(CacheEntryWithAfterToORM); ok {
		err = posthook.AfterToORM(ctx, &to)
	}
	return to, err
}

// ToPB runs the BeforeToPB hook if present, converts the fields of this
// object to PB format, runs the AfterToPB hook, then returns the PB object
func (m *CacheEntryORM) ToPB(ctx context.Context) (CacheEntry, error) {
	to := CacheEntry{}
	var err error
	if prehook, ok := interface{}(m).(CacheEntryWithBeforeToPB); ok {
		if err = prehook.BeforeToPB(ctx, &to); err != nil {
			return to, err
		}
	}
	to.Id = m.Id
	if m.CreatedAt != nil {
		if to.CreatedAt, err = ptypes1.TimestampProto(*m.CreatedAt); err != nil {
			return to, err
		}
	}
	if m.UpdatedAt != nil {
		if to.UpdatedAt, err = ptypes1.TimestampProto(*m.UpdatedAt); err != nil {
			return to, err
		}
	}
	to.Uri = m.Uri
	to.DownloadCount = m.DownloadCount
	to.RemoteID = m.RemoteID
	to.RefractID = m.RefractID
	if posthook, ok := interface{}(m).(CacheEntryWithAfterToPB); ok {
		err = posthook.AfterToPB(ctx, &to)
	}
	return to, err
}

// The following are interfaces you can implement for special behavior during ORM/PB conversions
// of type CacheEntry the arg will be the target, the caller the one being converted from

// CacheEntryBeforeToORM called before default ToORM code
type CacheEntryWithBeforeToORM interface {
	BeforeToORM(context.Context, *CacheEntryORM) error
}

// CacheEntryAfterToORM called after default ToORM code
type CacheEntryWithAfterToORM interface {
	AfterToORM(context.Context, *CacheEntryORM) error
}

// CacheEntryBeforeToPB called before default ToPB code
type CacheEntryWithBeforeToPB interface {
	BeforeToPB(context.Context, *CacheEntry) error
}

// CacheEntryAfterToPB called after default ToPB code
type CacheEntryWithAfterToPB interface {
	AfterToPB(context.Context, *CacheEntry) error
}

type CacheSliceORM struct {
	CreatedAt *time.Time
	EntryID   string `gorm:"type:uuid"`
	Hash      string
	Id        string `gorm:"type:uuid;primary_key"`
	UpdatedAt *time.Time
	Uri       string
}

// TableName overrides the default tablename generated by GORM
func (CacheSliceORM) TableName() string {
	return "cache_slices"
}

// ToORM runs the BeforeToORM hook if present, converts the fields of this
// object to ORM format, runs the AfterToORM hook, then returns the ORM object
func (m *CacheSlice) ToORM(ctx context.Context) (CacheSliceORM, error) {
	to := CacheSliceORM{}
	var err error
	if prehook, ok := interface{}(m).(CacheSliceWithBeforeToORM); ok {
		if err = prehook.BeforeToORM(ctx, &to); err != nil {
			return to, err
		}
	}
	to.Id = m.Id
	if m.CreatedAt != nil {
		var t time.Time
		if t, err = ptypes1.Timestamp(m.CreatedAt); err != nil {
			return to, err
		}
		to.CreatedAt = &t
	}
	if m.UpdatedAt != nil {
		var t time.Time
		if t, err = ptypes1.Timestamp(m.UpdatedAt); err != nil {
			return to, err
		}
		to.UpdatedAt = &t
	}
	to.EntryID = m.EntryID
	to.Uri = m.Uri
	to.Hash = m.Hash
	if posthook, ok := interface{}(m).(CacheSliceWithAfterToORM); ok {
		err = posthook.AfterToORM(ctx, &to)
	}
	return to, err
}

// ToPB runs the BeforeToPB hook if present, converts the fields of this
// object to PB format, runs the AfterToPB hook, then returns the PB object
func (m *CacheSliceORM) ToPB(ctx context.Context) (CacheSlice, error) {
	to := CacheSlice{}
	var err error
	if prehook, ok := interface{}(m).(CacheSliceWithBeforeToPB); ok {
		if err = prehook.BeforeToPB(ctx, &to); err != nil {
			return to, err
		}
	}
	to.Id = m.Id
	if m.CreatedAt != nil {
		if to.CreatedAt, err = ptypes1.TimestampProto(*m.CreatedAt); err != nil {
			return to, err
		}
	}
	if m.UpdatedAt != nil {
		if to.UpdatedAt, err = ptypes1.TimestampProto(*m.UpdatedAt); err != nil {
			return to, err
		}
	}
	to.EntryID = m.EntryID
	to.Uri = m.Uri
	to.Hash = m.Hash
	if posthook, ok := interface{}(m).(CacheSliceWithAfterToPB); ok {
		err = posthook.AfterToPB(ctx, &to)
	}
	return to, err
}

// The following are interfaces you can implement for special behavior during ORM/PB conversions
// of type CacheSlice the arg will be the target, the caller the one being converted from

// CacheSliceBeforeToORM called before default ToORM code
type CacheSliceWithBeforeToORM interface {
	BeforeToORM(context.Context, *CacheSliceORM) error
}

// CacheSliceAfterToORM called after default ToORM code
type CacheSliceWithAfterToORM interface {
	AfterToORM(context.Context, *CacheSliceORM) error
}

// CacheSliceBeforeToPB called before default ToPB code
type CacheSliceWithBeforeToPB interface {
	BeforeToPB(context.Context, *CacheSlice) error
}

// CacheSliceAfterToPB called after default ToPB code
type CacheSliceWithAfterToPB interface {
	AfterToPB(context.Context, *CacheSlice) error
}

// DefaultCreateCacheEntry executes a basic gorm create call
func DefaultCreateCacheEntry(ctx context.Context, in *CacheEntry, db *gorm1.DB) (*CacheEntry, error) {
	if in == nil {
		return nil, errors1.NilArgumentError
	}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithBeforeCreate_); ok {
		if db, err = hook.BeforeCreate_(ctx, db); err != nil {
			return nil, err
		}
	}
	if err = db.Create(&ormObj).Error; err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithAfterCreate_); ok {
		if err = hook.AfterCreate_(ctx, db); err != nil {
			return nil, err
		}
	}
	pbResponse, err := ormObj.ToPB(ctx)
	return &pbResponse, err
}

type CacheEntryORMWithBeforeCreate_ interface {
	BeforeCreate_(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithAfterCreate_ interface {
	AfterCreate_(context.Context, *gorm1.DB) error
}

// DefaultReadCacheEntry executes a basic gorm read call
func DefaultReadCacheEntry(ctx context.Context, in *CacheEntry, db *gorm1.DB) (*CacheEntry, error) {
	if in == nil {
		return nil, errors1.NilArgumentError
	}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return nil, err
	}
	if ormObj.Id == "" {
		return nil, errors1.EmptyIdError
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithBeforeReadApplyQuery); ok {
		if db, err = hook.BeforeReadApplyQuery(ctx, db); err != nil {
			return nil, err
		}
	}
	if db, err = gorm2.ApplyFieldSelection(ctx, db, nil, &CacheEntryORM{}); err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithBeforeReadFind); ok {
		if db, err = hook.BeforeReadFind(ctx, db); err != nil {
			return nil, err
		}
	}
	ormResponse := CacheEntryORM{}
	if err = db.Where(&ormObj).First(&ormResponse).Error; err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormResponse).(CacheEntryORMWithAfterReadFind); ok {
		if err = hook.AfterReadFind(ctx, db); err != nil {
			return nil, err
		}
	}
	pbResponse, err := ormResponse.ToPB(ctx)
	return &pbResponse, err
}

type CacheEntryORMWithBeforeReadApplyQuery interface {
	BeforeReadApplyQuery(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithBeforeReadFind interface {
	BeforeReadFind(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithAfterReadFind interface {
	AfterReadFind(context.Context, *gorm1.DB) error
}

func DefaultDeleteCacheEntry(ctx context.Context, in *CacheEntry, db *gorm1.DB) error {
	if in == nil {
		return errors1.NilArgumentError
	}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return err
	}
	if ormObj.Id == "" {
		return errors1.EmptyIdError
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithBeforeDelete_); ok {
		if db, err = hook.BeforeDelete_(ctx, db); err != nil {
			return err
		}
	}
	err = db.Where(&ormObj).Delete(&CacheEntryORM{}).Error
	if err != nil {
		return err
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithAfterDelete_); ok {
		err = hook.AfterDelete_(ctx, db)
	}
	return err
}

type CacheEntryORMWithBeforeDelete_ interface {
	BeforeDelete_(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithAfterDelete_ interface {
	AfterDelete_(context.Context, *gorm1.DB) error
}

func DefaultDeleteCacheEntrySet(ctx context.Context, in []*CacheEntry, db *gorm1.DB) error {
	if in == nil {
		return errors1.NilArgumentError
	}
	var err error
	keys := []string{}
	for _, obj := range in {
		ormObj, err := obj.ToORM(ctx)
		if err != nil {
			return err
		}
		if ormObj.Id == "" {
			return errors1.EmptyIdError
		}
		keys = append(keys, ormObj.Id)
	}
	if hook, ok := (interface{}(&CacheEntryORM{})).(CacheEntryORMWithBeforeDeleteSet); ok {
		if db, err = hook.BeforeDeleteSet(ctx, in, db); err != nil {
			return err
		}
	}
	err = db.Where("id in (?)", keys).Delete(&CacheEntryORM{}).Error
	if err != nil {
		return err
	}
	if hook, ok := (interface{}(&CacheEntryORM{})).(CacheEntryORMWithAfterDeleteSet); ok {
		err = hook.AfterDeleteSet(ctx, in, db)
	}
	return err
}

type CacheEntryORMWithBeforeDeleteSet interface {
	BeforeDeleteSet(context.Context, []*CacheEntry, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithAfterDeleteSet interface {
	AfterDeleteSet(context.Context, []*CacheEntry, *gorm1.DB) error
}

// DefaultStrictUpdateCacheEntry clears / replaces / appends first level 1:many children and then executes a gorm update call
func DefaultStrictUpdateCacheEntry(ctx context.Context, in *CacheEntry, db *gorm1.DB) (*CacheEntry, error) {
	if in == nil {
		return nil, fmt.Errorf("Nil argument to DefaultStrictUpdateCacheEntry")
	}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return nil, err
	}
	lockedRow := &CacheEntryORM{}
	db.Model(&ormObj).Set("gorm:query_option", "FOR UPDATE").Where("id=?", ormObj.Id).First(lockedRow)
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithBeforeStrictUpdateCleanup); ok {
		if db, err = hook.BeforeStrictUpdateCleanup(ctx, db); err != nil {
			return nil, err
		}
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithBeforeStrictUpdateSave); ok {
		if db, err = hook.BeforeStrictUpdateSave(ctx, db); err != nil {
			return nil, err
		}
	}
	if err = db.Save(&ormObj).Error; err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithAfterStrictUpdateSave); ok {
		if err = hook.AfterStrictUpdateSave(ctx, db); err != nil {
			return nil, err
		}
	}
	pbResponse, err := ormObj.ToPB(ctx)
	if err != nil {
		return nil, err
	}
	return &pbResponse, err
}

type CacheEntryORMWithBeforeStrictUpdateCleanup interface {
	BeforeStrictUpdateCleanup(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithBeforeStrictUpdateSave interface {
	BeforeStrictUpdateSave(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithAfterStrictUpdateSave interface {
	AfterStrictUpdateSave(context.Context, *gorm1.DB) error
}

// DefaultPatchCacheEntry executes a basic gorm update call with patch behavior
func DefaultPatchCacheEntry(ctx context.Context, in *CacheEntry, updateMask *field_mask1.FieldMask, db *gorm1.DB) (*CacheEntry, error) {
	if in == nil {
		return nil, errors1.NilArgumentError
	}
	var pbObj CacheEntry
	var err error
	if hook, ok := interface{}(&pbObj).(CacheEntryWithBeforePatchRead); ok {
		if db, err = hook.BeforePatchRead(ctx, in, updateMask, db); err != nil {
			return nil, err
		}
	}
	pbReadRes, err := DefaultReadCacheEntry(ctx, &CacheEntry{Id: in.GetId()}, db)
	if err != nil {
		return nil, err
	}
	pbObj = *pbReadRes
	if hook, ok := interface{}(&pbObj).(CacheEntryWithBeforePatchApplyFieldMask); ok {
		if db, err = hook.BeforePatchApplyFieldMask(ctx, in, updateMask, db); err != nil {
			return nil, err
		}
	}
	if _, err := DefaultApplyFieldMaskCacheEntry(ctx, &pbObj, in, updateMask, "", db); err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&pbObj).(CacheEntryWithBeforePatchSave); ok {
		if db, err = hook.BeforePatchSave(ctx, in, updateMask, db); err != nil {
			return nil, err
		}
	}
	pbResponse, err := DefaultStrictUpdateCacheEntry(ctx, &pbObj, db)
	if err != nil {
		return nil, err
	}
	if hook, ok := interface{}(pbResponse).(CacheEntryWithAfterPatchSave); ok {
		if err = hook.AfterPatchSave(ctx, in, updateMask, db); err != nil {
			return nil, err
		}
	}
	return pbResponse, nil
}

type CacheEntryWithBeforePatchRead interface {
	BeforePatchRead(context.Context, *CacheEntry, *field_mask1.FieldMask, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryWithBeforePatchApplyFieldMask interface {
	BeforePatchApplyFieldMask(context.Context, *CacheEntry, *field_mask1.FieldMask, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryWithBeforePatchSave interface {
	BeforePatchSave(context.Context, *CacheEntry, *field_mask1.FieldMask, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryWithAfterPatchSave interface {
	AfterPatchSave(context.Context, *CacheEntry, *field_mask1.FieldMask, *gorm1.DB) error
}

// DefaultPatchSetCacheEntry executes a bulk gorm update call with patch behavior
func DefaultPatchSetCacheEntry(ctx context.Context, objects []*CacheEntry, updateMasks []*field_mask1.FieldMask, db *gorm1.DB) ([]*CacheEntry, error) {
	if len(objects) != len(updateMasks) {
		return nil, fmt.Errorf(errors1.BadRepeatedFieldMaskTpl, len(updateMasks), len(objects))
	}

	results := make([]*CacheEntry, 0, len(objects))
	for i, patcher := range objects {
		pbResponse, err := DefaultPatchCacheEntry(ctx, patcher, updateMasks[i], db)
		if err != nil {
			return nil, err
		}

		results = append(results, pbResponse)
	}

	return results, nil
}

// DefaultApplyFieldMaskCacheEntry patches an pbObject with patcher according to a field mask.
func DefaultApplyFieldMaskCacheEntry(ctx context.Context, patchee *CacheEntry, patcher *CacheEntry, updateMask *field_mask1.FieldMask, prefix string, db *gorm1.DB) (*CacheEntry, error) {
	if patcher == nil {
		return nil, nil
	} else if patchee == nil {
		return nil, errors1.NilArgumentError
	}
	var err error
	for _, f := range updateMask.Paths {
		if f == prefix+"Id" {
			patchee.Id = patcher.Id
			continue
		}
		if f == prefix+"CreatedAt" {
			patchee.CreatedAt = patcher.CreatedAt
			continue
		}
		if f == prefix+"UpdatedAt" {
			patchee.UpdatedAt = patcher.UpdatedAt
			continue
		}
		if f == prefix+"Uri" {
			patchee.Uri = patcher.Uri
			continue
		}
		if f == prefix+"DownloadCount" {
			patchee.DownloadCount = patcher.DownloadCount
			continue
		}
		if f == prefix+"RemoteID" {
			patchee.RemoteID = patcher.RemoteID
			continue
		}
		if f == prefix+"RefractID" {
			patchee.RefractID = patcher.RefractID
			continue
		}
	}
	if err != nil {
		return nil, err
	}
	return patchee, nil
}

// DefaultListCacheEntry executes a gorm list call
func DefaultListCacheEntry(ctx context.Context, db *gorm1.DB) ([]*CacheEntry, error) {
	in := CacheEntry{}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithBeforeListApplyQuery); ok {
		if db, err = hook.BeforeListApplyQuery(ctx, db); err != nil {
			return nil, err
		}
	}
	db, err = gorm2.ApplyCollectionOperators(ctx, db, &CacheEntryORM{}, &CacheEntry{}, nil, nil, nil, nil)
	if err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithBeforeListFind); ok {
		if db, err = hook.BeforeListFind(ctx, db); err != nil {
			return nil, err
		}
	}
	db = db.Where(&ormObj)
	db = db.Order("id")
	ormResponse := []CacheEntryORM{}
	if err := db.Find(&ormResponse).Error; err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheEntryORMWithAfterListFind); ok {
		if err = hook.AfterListFind(ctx, db, &ormResponse); err != nil {
			return nil, err
		}
	}
	pbResponse := []*CacheEntry{}
	for _, responseEntry := range ormResponse {
		temp, err := responseEntry.ToPB(ctx)
		if err != nil {
			return nil, err
		}
		pbResponse = append(pbResponse, &temp)
	}
	return pbResponse, nil
}

type CacheEntryORMWithBeforeListApplyQuery interface {
	BeforeListApplyQuery(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithBeforeListFind interface {
	BeforeListFind(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheEntryORMWithAfterListFind interface {
	AfterListFind(context.Context, *gorm1.DB, *[]CacheEntryORM) error
}

// DefaultCreateCacheSlice executes a basic gorm create call
func DefaultCreateCacheSlice(ctx context.Context, in *CacheSlice, db *gorm1.DB) (*CacheSlice, error) {
	if in == nil {
		return nil, errors1.NilArgumentError
	}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithBeforeCreate_); ok {
		if db, err = hook.BeforeCreate_(ctx, db); err != nil {
			return nil, err
		}
	}
	if err = db.Create(&ormObj).Error; err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithAfterCreate_); ok {
		if err = hook.AfterCreate_(ctx, db); err != nil {
			return nil, err
		}
	}
	pbResponse, err := ormObj.ToPB(ctx)
	return &pbResponse, err
}

type CacheSliceORMWithBeforeCreate_ interface {
	BeforeCreate_(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithAfterCreate_ interface {
	AfterCreate_(context.Context, *gorm1.DB) error
}

// DefaultReadCacheSlice executes a basic gorm read call
func DefaultReadCacheSlice(ctx context.Context, in *CacheSlice, db *gorm1.DB) (*CacheSlice, error) {
	if in == nil {
		return nil, errors1.NilArgumentError
	}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return nil, err
	}
	if ormObj.Id == "" {
		return nil, errors1.EmptyIdError
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithBeforeReadApplyQuery); ok {
		if db, err = hook.BeforeReadApplyQuery(ctx, db); err != nil {
			return nil, err
		}
	}
	if db, err = gorm2.ApplyFieldSelection(ctx, db, nil, &CacheSliceORM{}); err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithBeforeReadFind); ok {
		if db, err = hook.BeforeReadFind(ctx, db); err != nil {
			return nil, err
		}
	}
	ormResponse := CacheSliceORM{}
	if err = db.Where(&ormObj).First(&ormResponse).Error; err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormResponse).(CacheSliceORMWithAfterReadFind); ok {
		if err = hook.AfterReadFind(ctx, db); err != nil {
			return nil, err
		}
	}
	pbResponse, err := ormResponse.ToPB(ctx)
	return &pbResponse, err
}

type CacheSliceORMWithBeforeReadApplyQuery interface {
	BeforeReadApplyQuery(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithBeforeReadFind interface {
	BeforeReadFind(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithAfterReadFind interface {
	AfterReadFind(context.Context, *gorm1.DB) error
}

func DefaultDeleteCacheSlice(ctx context.Context, in *CacheSlice, db *gorm1.DB) error {
	if in == nil {
		return errors1.NilArgumentError
	}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return err
	}
	if ormObj.Id == "" {
		return errors1.EmptyIdError
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithBeforeDelete_); ok {
		if db, err = hook.BeforeDelete_(ctx, db); err != nil {
			return err
		}
	}
	err = db.Where(&ormObj).Delete(&CacheSliceORM{}).Error
	if err != nil {
		return err
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithAfterDelete_); ok {
		err = hook.AfterDelete_(ctx, db)
	}
	return err
}

type CacheSliceORMWithBeforeDelete_ interface {
	BeforeDelete_(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithAfterDelete_ interface {
	AfterDelete_(context.Context, *gorm1.DB) error
}

func DefaultDeleteCacheSliceSet(ctx context.Context, in []*CacheSlice, db *gorm1.DB) error {
	if in == nil {
		return errors1.NilArgumentError
	}
	var err error
	keys := []string{}
	for _, obj := range in {
		ormObj, err := obj.ToORM(ctx)
		if err != nil {
			return err
		}
		if ormObj.Id == "" {
			return errors1.EmptyIdError
		}
		keys = append(keys, ormObj.Id)
	}
	if hook, ok := (interface{}(&CacheSliceORM{})).(CacheSliceORMWithBeforeDeleteSet); ok {
		if db, err = hook.BeforeDeleteSet(ctx, in, db); err != nil {
			return err
		}
	}
	err = db.Where("id in (?)", keys).Delete(&CacheSliceORM{}).Error
	if err != nil {
		return err
	}
	if hook, ok := (interface{}(&CacheSliceORM{})).(CacheSliceORMWithAfterDeleteSet); ok {
		err = hook.AfterDeleteSet(ctx, in, db)
	}
	return err
}

type CacheSliceORMWithBeforeDeleteSet interface {
	BeforeDeleteSet(context.Context, []*CacheSlice, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithAfterDeleteSet interface {
	AfterDeleteSet(context.Context, []*CacheSlice, *gorm1.DB) error
}

// DefaultStrictUpdateCacheSlice clears / replaces / appends first level 1:many children and then executes a gorm update call
func DefaultStrictUpdateCacheSlice(ctx context.Context, in *CacheSlice, db *gorm1.DB) (*CacheSlice, error) {
	if in == nil {
		return nil, fmt.Errorf("Nil argument to DefaultStrictUpdateCacheSlice")
	}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return nil, err
	}
	lockedRow := &CacheSliceORM{}
	db.Model(&ormObj).Set("gorm:query_option", "FOR UPDATE").Where("id=?", ormObj.Id).First(lockedRow)
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithBeforeStrictUpdateCleanup); ok {
		if db, err = hook.BeforeStrictUpdateCleanup(ctx, db); err != nil {
			return nil, err
		}
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithBeforeStrictUpdateSave); ok {
		if db, err = hook.BeforeStrictUpdateSave(ctx, db); err != nil {
			return nil, err
		}
	}
	if err = db.Save(&ormObj).Error; err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithAfterStrictUpdateSave); ok {
		if err = hook.AfterStrictUpdateSave(ctx, db); err != nil {
			return nil, err
		}
	}
	pbResponse, err := ormObj.ToPB(ctx)
	if err != nil {
		return nil, err
	}
	return &pbResponse, err
}

type CacheSliceORMWithBeforeStrictUpdateCleanup interface {
	BeforeStrictUpdateCleanup(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithBeforeStrictUpdateSave interface {
	BeforeStrictUpdateSave(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithAfterStrictUpdateSave interface {
	AfterStrictUpdateSave(context.Context, *gorm1.DB) error
}

// DefaultPatchCacheSlice executes a basic gorm update call with patch behavior
func DefaultPatchCacheSlice(ctx context.Context, in *CacheSlice, updateMask *field_mask1.FieldMask, db *gorm1.DB) (*CacheSlice, error) {
	if in == nil {
		return nil, errors1.NilArgumentError
	}
	var pbObj CacheSlice
	var err error
	if hook, ok := interface{}(&pbObj).(CacheSliceWithBeforePatchRead); ok {
		if db, err = hook.BeforePatchRead(ctx, in, updateMask, db); err != nil {
			return nil, err
		}
	}
	pbReadRes, err := DefaultReadCacheSlice(ctx, &CacheSlice{Id: in.GetId()}, db)
	if err != nil {
		return nil, err
	}
	pbObj = *pbReadRes
	if hook, ok := interface{}(&pbObj).(CacheSliceWithBeforePatchApplyFieldMask); ok {
		if db, err = hook.BeforePatchApplyFieldMask(ctx, in, updateMask, db); err != nil {
			return nil, err
		}
	}
	if _, err := DefaultApplyFieldMaskCacheSlice(ctx, &pbObj, in, updateMask, "", db); err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&pbObj).(CacheSliceWithBeforePatchSave); ok {
		if db, err = hook.BeforePatchSave(ctx, in, updateMask, db); err != nil {
			return nil, err
		}
	}
	pbResponse, err := DefaultStrictUpdateCacheSlice(ctx, &pbObj, db)
	if err != nil {
		return nil, err
	}
	if hook, ok := interface{}(pbResponse).(CacheSliceWithAfterPatchSave); ok {
		if err = hook.AfterPatchSave(ctx, in, updateMask, db); err != nil {
			return nil, err
		}
	}
	return pbResponse, nil
}

type CacheSliceWithBeforePatchRead interface {
	BeforePatchRead(context.Context, *CacheSlice, *field_mask1.FieldMask, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceWithBeforePatchApplyFieldMask interface {
	BeforePatchApplyFieldMask(context.Context, *CacheSlice, *field_mask1.FieldMask, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceWithBeforePatchSave interface {
	BeforePatchSave(context.Context, *CacheSlice, *field_mask1.FieldMask, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceWithAfterPatchSave interface {
	AfterPatchSave(context.Context, *CacheSlice, *field_mask1.FieldMask, *gorm1.DB) error
}

// DefaultPatchSetCacheSlice executes a bulk gorm update call with patch behavior
func DefaultPatchSetCacheSlice(ctx context.Context, objects []*CacheSlice, updateMasks []*field_mask1.FieldMask, db *gorm1.DB) ([]*CacheSlice, error) {
	if len(objects) != len(updateMasks) {
		return nil, fmt.Errorf(errors1.BadRepeatedFieldMaskTpl, len(updateMasks), len(objects))
	}

	results := make([]*CacheSlice, 0, len(objects))
	for i, patcher := range objects {
		pbResponse, err := DefaultPatchCacheSlice(ctx, patcher, updateMasks[i], db)
		if err != nil {
			return nil, err
		}

		results = append(results, pbResponse)
	}

	return results, nil
}

// DefaultApplyFieldMaskCacheSlice patches an pbObject with patcher according to a field mask.
func DefaultApplyFieldMaskCacheSlice(ctx context.Context, patchee *CacheSlice, patcher *CacheSlice, updateMask *field_mask1.FieldMask, prefix string, db *gorm1.DB) (*CacheSlice, error) {
	if patcher == nil {
		return nil, nil
	} else if patchee == nil {
		return nil, errors1.NilArgumentError
	}
	var err error
	for _, f := range updateMask.Paths {
		if f == prefix+"Id" {
			patchee.Id = patcher.Id
			continue
		}
		if f == prefix+"CreatedAt" {
			patchee.CreatedAt = patcher.CreatedAt
			continue
		}
		if f == prefix+"UpdatedAt" {
			patchee.UpdatedAt = patcher.UpdatedAt
			continue
		}
		if f == prefix+"EntryID" {
			patchee.EntryID = patcher.EntryID
			continue
		}
		if f == prefix+"Uri" {
			patchee.Uri = patcher.Uri
			continue
		}
		if f == prefix+"Hash" {
			patchee.Hash = patcher.Hash
			continue
		}
	}
	if err != nil {
		return nil, err
	}
	return patchee, nil
}

// DefaultListCacheSlice executes a gorm list call
func DefaultListCacheSlice(ctx context.Context, db *gorm1.DB) ([]*CacheSlice, error) {
	in := CacheSlice{}
	ormObj, err := in.ToORM(ctx)
	if err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithBeforeListApplyQuery); ok {
		if db, err = hook.BeforeListApplyQuery(ctx, db); err != nil {
			return nil, err
		}
	}
	db, err = gorm2.ApplyCollectionOperators(ctx, db, &CacheSliceORM{}, &CacheSlice{}, nil, nil, nil, nil)
	if err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithBeforeListFind); ok {
		if db, err = hook.BeforeListFind(ctx, db); err != nil {
			return nil, err
		}
	}
	db = db.Where(&ormObj)
	db = db.Order("id")
	ormResponse := []CacheSliceORM{}
	if err := db.Find(&ormResponse).Error; err != nil {
		return nil, err
	}
	if hook, ok := interface{}(&ormObj).(CacheSliceORMWithAfterListFind); ok {
		if err = hook.AfterListFind(ctx, db, &ormResponse); err != nil {
			return nil, err
		}
	}
	pbResponse := []*CacheSlice{}
	for _, responseEntry := range ormResponse {
		temp, err := responseEntry.ToPB(ctx)
		if err != nil {
			return nil, err
		}
		pbResponse = append(pbResponse, &temp)
	}
	return pbResponse, nil
}

type CacheSliceORMWithBeforeListApplyQuery interface {
	BeforeListApplyQuery(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithBeforeListFind interface {
	BeforeListFind(context.Context, *gorm1.DB) (*gorm1.DB, error)
}
type CacheSliceORMWithAfterListFind interface {
	AfterListFind(context.Context, *gorm1.DB, *[]CacheSliceORM) error
}
