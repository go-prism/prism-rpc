// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.15.8
// source: service/api/refract.proto

package api

import (
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type GetRefractRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
}

func (x *GetRefractRequest) Reset() {
	*x = GetRefractRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_service_api_refract_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetRefractRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetRefractRequest) ProtoMessage() {}

func (x *GetRefractRequest) ProtoReflect() protoreflect.Message {
	mi := &file_service_api_refract_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetRefractRequest.ProtoReflect.Descriptor instead.
func (*GetRefractRequest) Descriptor() ([]byte, []int) {
	return file_service_api_refract_proto_rawDescGZIP(), []int{0}
}

func (x *GetRefractRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type CreateRefractRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name      string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name"`
	Archetype string   `protobuf:"bytes,2,opt,name=archetype,proto3" json:"archetype"`
	Remotes   []string `protobuf:"bytes,3,rep,name=remotes,json=remotesList,proto3" json:"remotes"`
}

func (x *CreateRefractRequest) Reset() {
	*x = CreateRefractRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_service_api_refract_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateRefractRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateRefractRequest) ProtoMessage() {}

func (x *CreateRefractRequest) ProtoReflect() protoreflect.Message {
	mi := &file_service_api_refract_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateRefractRequest.ProtoReflect.Descriptor instead.
func (*CreateRefractRequest) Descriptor() ([]byte, []int) {
	return file_service_api_refract_proto_rawDescGZIP(), []int{1}
}

func (x *CreateRefractRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CreateRefractRequest) GetArchetype() string {
	if x != nil {
		return x.Archetype
	}
	return ""
}

func (x *CreateRefractRequest) GetRemotes() []string {
	if x != nil {
		return x.Remotes
	}
	return nil
}

type PatchRefractRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id  string                `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	Req *CreateRefractRequest `protobuf:"bytes,2,opt,name=req,proto3" json:"req"`
}

func (x *PatchRefractRequest) Reset() {
	*x = PatchRefractRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_service_api_refract_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PatchRefractRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PatchRefractRequest) ProtoMessage() {}

func (x *PatchRefractRequest) ProtoReflect() protoreflect.Message {
	mi := &file_service_api_refract_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PatchRefractRequest.ProtoReflect.Descriptor instead.
func (*PatchRefractRequest) Descriptor() ([]byte, []int) {
	return file_service_api_refract_proto_rawDescGZIP(), []int{2}
}

func (x *PatchRefractRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *PatchRefractRequest) GetReq() *CreateRefractRequest {
	if x != nil {
		return x.Req
	}
	return nil
}

type DeleteRefractRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
}

func (x *DeleteRefractRequest) Reset() {
	*x = DeleteRefractRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_service_api_refract_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteRefractRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteRefractRequest) ProtoMessage() {}

func (x *DeleteRefractRequest) ProtoReflect() protoreflect.Message {
	mi := &file_service_api_refract_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteRefractRequest.ProtoReflect.Descriptor instead.
func (*DeleteRefractRequest) Descriptor() ([]byte, []int) {
	return file_service_api_refract_proto_rawDescGZIP(), []int{3}
}

func (x *DeleteRefractRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

var File_service_api_refract_proto protoreflect.FileDescriptor

var file_service_api_refract_proto_rawDesc = []byte{
	0x0a, 0x19, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x72, 0x65,
	0x66, 0x72, 0x61, 0x63, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x03, 0x61, 0x70, 0x69,
	0x1a, 0x17, 0x64, 0x6f, 0x6d, 0x61, 0x69, 0x6e, 0x2f, 0x76, 0x31, 0x2f, 0x72, 0x65, 0x66, 0x72,
	0x61, 0x63, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x23, 0x0a, 0x11, 0x47, 0x65, 0x74, 0x52, 0x65, 0x66,
	0x72, 0x61, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x66, 0x0a, 0x14, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x61, 0x72, 0x63, 0x68, 0x65,
	0x74, 0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x61, 0x72, 0x63, 0x68,
	0x65, 0x74, 0x79, 0x70, 0x65, 0x12, 0x1c, 0x0a, 0x07, 0x72, 0x65, 0x6d, 0x6f, 0x74, 0x65, 0x73,
	0x18, 0x03, 0x20, 0x03, 0x28, 0x09, 0x52, 0x0b, 0x72, 0x65, 0x6d, 0x6f, 0x74, 0x65, 0x73, 0x4c,
	0x69, 0x73, 0x74, 0x22, 0x52, 0x0a, 0x13, 0x50, 0x61, 0x74, 0x63, 0x68, 0x52, 0x65, 0x66, 0x72,
	0x61, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x2b, 0x0a, 0x03, 0x72, 0x65,
	0x71, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x19, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x43, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x52, 0x03, 0x72, 0x65, 0x71, 0x22, 0x26, 0x0a, 0x14, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x32,
	0x9a, 0x02, 0x0a, 0x0b, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x12,
	0x2f, 0x0a, 0x04, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a,
	0x0f, 0x2e, 0x76, 0x31, 0x2e, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x73,
	0x12, 0x2d, 0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x16, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x47, 0x65,
	0x74, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x0e, 0x2e, 0x76, 0x31, 0x2e, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x12,
	0x33, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x19, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x0e, 0x2e, 0x76, 0x31, 0x2e, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x12, 0x39, 0x0a, 0x05, 0x50, 0x61, 0x74, 0x63, 0x68, 0x12, 0x18, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x50, 0x61, 0x74, 0x63, 0x68, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x12,
	0x3b, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x19, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x52, 0x65, 0x66, 0x72, 0x61, 0x63, 0x74, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x42, 0x2b, 0x5a, 0x29,
	0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x67, 0x6f, 0x2d, 0x70, 0x72,
	0x69, 0x73, 0x6d, 0x2f, 0x70, 0x72, 0x69, 0x73, 0x6d, 0x2d, 0x72, 0x70, 0x63, 0x2f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x33,
}

var (
	file_service_api_refract_proto_rawDescOnce sync.Once
	file_service_api_refract_proto_rawDescData = file_service_api_refract_proto_rawDesc
)

func file_service_api_refract_proto_rawDescGZIP() []byte {
	file_service_api_refract_proto_rawDescOnce.Do(func() {
		file_service_api_refract_proto_rawDescData = protoimpl.X.CompressGZIP(file_service_api_refract_proto_rawDescData)
	})
	return file_service_api_refract_proto_rawDescData
}

var file_service_api_refract_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_service_api_refract_proto_goTypes = []interface{}{
	(*GetRefractRequest)(nil),    // 0: api.GetRefractRequest
	(*CreateRefractRequest)(nil), // 1: api.CreateRefractRequest
	(*PatchRefractRequest)(nil),  // 2: api.PatchRefractRequest
	(*DeleteRefractRequest)(nil), // 3: api.DeleteRefractRequest
	(*emptypb.Empty)(nil),        // 4: google.protobuf.Empty
	(*v1.Refractions)(nil),       // 5: v1.Refractions
	(*v1.Refraction)(nil),        // 6: v1.Refraction
}
var file_service_api_refract_proto_depIdxs = []int32{
	1, // 0: api.PatchRefractRequest.req:type_name -> api.CreateRefractRequest
	4, // 1: api.Refractions.List:input_type -> google.protobuf.Empty
	0, // 2: api.Refractions.Get:input_type -> api.GetRefractRequest
	1, // 3: api.Refractions.Create:input_type -> api.CreateRefractRequest
	2, // 4: api.Refractions.Patch:input_type -> api.PatchRefractRequest
	3, // 5: api.Refractions.Delete:input_type -> api.DeleteRefractRequest
	5, // 6: api.Refractions.List:output_type -> v1.Refractions
	6, // 7: api.Refractions.Get:output_type -> v1.Refraction
	6, // 8: api.Refractions.Create:output_type -> v1.Refraction
	4, // 9: api.Refractions.Patch:output_type -> google.protobuf.Empty
	4, // 10: api.Refractions.Delete:output_type -> google.protobuf.Empty
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_service_api_refract_proto_init() }
func file_service_api_refract_proto_init() {
	if File_service_api_refract_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_service_api_refract_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetRefractRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_service_api_refract_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateRefractRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_service_api_refract_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PatchRefractRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_service_api_refract_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeleteRefractRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_service_api_refract_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_service_api_refract_proto_goTypes,
		DependencyIndexes: file_service_api_refract_proto_depIdxs,
		MessageInfos:      file_service_api_refract_proto_msgTypes,
	}.Build()
	File_service_api_refract_proto = out.File
	file_service_api_refract_proto_rawDesc = nil
	file_service_api_refract_proto_goTypes = nil
	file_service_api_refract_proto_depIdxs = nil
}
