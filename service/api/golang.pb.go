// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.15.8
// source: service/api/golang.proto

package api

import (
	archv1 "gitlab.com/go-prism/prism-rpc/domain/archv1"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ListModRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name"`
}

func (x *ListModRequest) Reset() {
	*x = ListModRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_service_api_golang_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListModRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListModRequest) ProtoMessage() {}

func (x *ListModRequest) ProtoReflect() protoreflect.Message {
	mi := &file_service_api_golang_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListModRequest.ProtoReflect.Descriptor instead.
func (*ListModRequest) Descriptor() ([]byte, []int) {
	return file_service_api_golang_proto_rawDescGZIP(), []int{0}
}

func (x *ListModRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

type GetModRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name    string `protobuf:"bytes,1,opt,name=name,proto3" json:"name"`
	Version string `protobuf:"bytes,2,opt,name=version,proto3" json:"version"`
}

func (x *GetModRequest) Reset() {
	*x = GetModRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_service_api_golang_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetModRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetModRequest) ProtoMessage() {}

func (x *GetModRequest) ProtoReflect() protoreflect.Message {
	mi := &file_service_api_golang_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetModRequest.ProtoReflect.Descriptor instead.
func (*GetModRequest) Descriptor() ([]byte, []int) {
	return file_service_api_golang_proto_rawDescGZIP(), []int{1}
}

func (x *GetModRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *GetModRequest) GetVersion() string {
	if x != nil {
		return x.Version
	}
	return ""
}

type CreateModRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name      string `protobuf:"bytes,1,opt,name=name,proto3" json:"name"`
	Version   string `protobuf:"bytes,2,opt,name=version,proto3" json:"version"`
	Timestamp string `protobuf:"bytes,3,opt,name=timestamp,proto3" json:"timestamp"`
}

func (x *CreateModRequest) Reset() {
	*x = CreateModRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_service_api_golang_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateModRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateModRequest) ProtoMessage() {}

func (x *CreateModRequest) ProtoReflect() protoreflect.Message {
	mi := &file_service_api_golang_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateModRequest.ProtoReflect.Descriptor instead.
func (*CreateModRequest) Descriptor() ([]byte, []int) {
	return file_service_api_golang_proto_rawDescGZIP(), []int{2}
}

func (x *CreateModRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CreateModRequest) GetVersion() string {
	if x != nil {
		return x.Version
	}
	return ""
}

func (x *CreateModRequest) GetTimestamp() string {
	if x != nil {
		return x.Timestamp
	}
	return ""
}

type GetRuleRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Source string `protobuf:"bytes,1,opt,name=source,proto3" json:"source"`
}

func (x *GetRuleRequest) Reset() {
	*x = GetRuleRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_service_api_golang_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetRuleRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetRuleRequest) ProtoMessage() {}

func (x *GetRuleRequest) ProtoReflect() protoreflect.Message {
	mi := &file_service_api_golang_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetRuleRequest.ProtoReflect.Descriptor instead.
func (*GetRuleRequest) Descriptor() ([]byte, []int) {
	return file_service_api_golang_proto_rawDescGZIP(), []int{3}
}

func (x *GetRuleRequest) GetSource() string {
	if x != nil {
		return x.Source
	}
	return ""
}

var File_service_api_golang_proto protoreflect.FileDescriptor

var file_service_api_golang_proto_rawDesc = []byte{
	0x0a, 0x18, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x67, 0x6f,
	0x6c, 0x61, 0x6e, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x03, 0x61, 0x70, 0x69, 0x1a,
	0x1a, 0x64, 0x6f, 0x6d, 0x61, 0x69, 0x6e, 0x2f, 0x61, 0x72, 0x63, 0x68, 0x76, 0x31, 0x2f, 0x67,
	0x6f, 0x6c, 0x61, 0x6e, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x24, 0x0a, 0x0e, 0x4c,
	0x69, 0x73, 0x74, 0x4d, 0x6f, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x22, 0x3d, 0x0a, 0x0d, 0x47, 0x65, 0x74, 0x4d, 0x6f, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f,
	0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e,
	0x22, 0x5e, 0x0a, 0x10, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x4d, 0x6f, 0x64, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x76, 0x65, 0x72, 0x73,
	0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x76, 0x65, 0x72, 0x73, 0x69,
	0x6f, 0x6e, 0x12, 0x1c, 0x0a, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70,
	0x22, 0x28, 0x0a, 0x0e, 0x47, 0x65, 0x74, 0x52, 0x75, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x32, 0xa8, 0x01, 0x0a, 0x07, 0x4d,
	0x6f, 0x64, 0x75, 0x6c, 0x65, 0x73, 0x12, 0x33, 0x0a, 0x04, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x13,
	0x2e, 0x61, 0x70, 0x69, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x4d, 0x6f, 0x64, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x61, 0x72, 0x63, 0x68, 0x76, 0x31, 0x2e, 0x4d, 0x6f, 0x64,
	0x75, 0x6c, 0x65, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x73, 0x12, 0x30, 0x0a, 0x03, 0x47,
	0x65, 0x74, 0x12, 0x12, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x47, 0x65, 0x74, 0x4d, 0x6f, 0x64, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e, 0x61, 0x72, 0x63, 0x68, 0x76, 0x31, 0x2e,
	0x4d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x12, 0x36, 0x0a,
	0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x15, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x43, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x4d, 0x6f, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15,
	0x2e, 0x61, 0x72, 0x63, 0x68, 0x76, 0x31, 0x2e, 0x4d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x56, 0x65,
	0x72, 0x73, 0x69, 0x6f, 0x6e, 0x32, 0x38, 0x0a, 0x05, 0x52, 0x75, 0x6c, 0x65, 0x73, 0x12, 0x2f,
	0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x13, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x47, 0x65, 0x74, 0x52,
	0x75, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x13, 0x2e, 0x61, 0x72, 0x63,
	0x68, 0x76, 0x31, 0x2e, 0x48, 0x6f, 0x73, 0x74, 0x52, 0x65, 0x77, 0x72, 0x69, 0x74, 0x65, 0x42,
	0x2b, 0x5a, 0x29, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x67, 0x6f,
	0x2d, 0x70, 0x72, 0x69, 0x73, 0x6d, 0x2f, 0x70, 0x72, 0x69, 0x73, 0x6d, 0x2d, 0x72, 0x70, 0x63,
	0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_service_api_golang_proto_rawDescOnce sync.Once
	file_service_api_golang_proto_rawDescData = file_service_api_golang_proto_rawDesc
)

func file_service_api_golang_proto_rawDescGZIP() []byte {
	file_service_api_golang_proto_rawDescOnce.Do(func() {
		file_service_api_golang_proto_rawDescData = protoimpl.X.CompressGZIP(file_service_api_golang_proto_rawDescData)
	})
	return file_service_api_golang_proto_rawDescData
}

var file_service_api_golang_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_service_api_golang_proto_goTypes = []interface{}{
	(*ListModRequest)(nil),        // 0: api.ListModRequest
	(*GetModRequest)(nil),         // 1: api.GetModRequest
	(*CreateModRequest)(nil),      // 2: api.CreateModRequest
	(*GetRuleRequest)(nil),        // 3: api.GetRuleRequest
	(*archv1.ModuleVersions)(nil), // 4: archv1.ModuleVersions
	(*archv1.ModuleVersion)(nil),  // 5: archv1.ModuleVersion
	(*archv1.HostRewrite)(nil),    // 6: archv1.HostRewrite
}
var file_service_api_golang_proto_depIdxs = []int32{
	0, // 0: api.Modules.List:input_type -> api.ListModRequest
	1, // 1: api.Modules.Get:input_type -> api.GetModRequest
	2, // 2: api.Modules.Create:input_type -> api.CreateModRequest
	3, // 3: api.Rules.Get:input_type -> api.GetRuleRequest
	4, // 4: api.Modules.List:output_type -> archv1.ModuleVersions
	5, // 5: api.Modules.Get:output_type -> archv1.ModuleVersion
	5, // 6: api.Modules.Create:output_type -> archv1.ModuleVersion
	6, // 7: api.Rules.Get:output_type -> archv1.HostRewrite
	4, // [4:8] is the sub-list for method output_type
	0, // [0:4] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_service_api_golang_proto_init() }
func file_service_api_golang_proto_init() {
	if File_service_api_golang_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_service_api_golang_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListModRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_service_api_golang_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetModRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_service_api_golang_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateModRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_service_api_golang_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetRuleRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_service_api_golang_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   2,
		},
		GoTypes:           file_service_api_golang_proto_goTypes,
		DependencyIndexes: file_service_api_golang_proto_depIdxs,
		MessageInfos:      file_service_api_golang_proto_msgTypes,
	}.Build()
	File_service_api_golang_proto = out.File
	file_service_api_golang_proto_rawDesc = nil
	file_service_api_golang_proto_goTypes = nil
	file_service_api_golang_proto_depIdxs = nil
}
