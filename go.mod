module gitlab.com/go-prism/prism-rpc

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0 // indirect
	github.com/denisenkom/go-mssqldb v0.9.0 // indirect
	github.com/djcass44/go-tracer v0.3.0
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.14.6 // indirect
	github.com/infobloxopen/atlas-app-toolkit v0.0.0-20180810144038-3237ef051031
	github.com/infobloxopen/protoc-gen-gorm v0.20.1
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/lib/pq v1.1.1
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	github.com/prometheus/client_golang v1.10.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d
	google.golang.org/genproto v0.0.0-20210406143921-e86de6bf7a46
	google.golang.org/grpc v1.37.0
	google.golang.org/grpc/examples v0.0.0-20210409234925-fab5982df20a // indirect
	google.golang.org/protobuf v1.26.0
)
