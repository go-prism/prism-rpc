// package: archv1
// file: domain/archv1/maven.proto

import * as jspb from "google-protobuf";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";

export class MavenPOM extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getArtifactid(): string;
  setArtifactid(value: string): void;

  getGroupid(): string;
  setGroupid(value: string): void;

  getVersion(): string;
  setVersion(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getFile(): string;
  setFile(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MavenPOM.AsObject;
  static toObject(includeInstance: boolean, msg: MavenPOM): MavenPOM.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MavenPOM, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MavenPOM;
  static deserializeBinaryFromReader(message: MavenPOM, reader: jspb.BinaryReader): MavenPOM;
}

export namespace MavenPOM {
  export type AsObject = {
    id: string,
    artifactid: string,
    groupid: string,
    version: string,
    description: string,
    file: string,
  }
}

