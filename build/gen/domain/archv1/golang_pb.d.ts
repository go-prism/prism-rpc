// package: archv1
// file: domain/archv1/golang.proto

import * as jspb from "google-protobuf";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";

export class ModuleVersion extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getModulename(): string;
  setModulename(value: string): void;

  getVersionname(): string;
  setVersionname(value: string): void;

  getTimestamp(): string;
  setTimestamp(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ModuleVersion.AsObject;
  static toObject(includeInstance: boolean, msg: ModuleVersion): ModuleVersion.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ModuleVersion, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ModuleVersion;
  static deserializeBinaryFromReader(message: ModuleVersion, reader: jspb.BinaryReader): ModuleVersion;
}

export namespace ModuleVersion {
  export type AsObject = {
    id: string,
    modulename: string,
    versionname: string,
    timestamp: string,
  }
}

export class ModuleVersions extends jspb.Message {
  clearVersionsList(): void;
  getVersionsList(): Array<ModuleVersion>;
  setVersionsList(value: Array<ModuleVersion>): void;
  addVersions(value?: ModuleVersion, index?: number): ModuleVersion;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ModuleVersions.AsObject;
  static toObject(includeInstance: boolean, msg: ModuleVersions): ModuleVersions.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ModuleVersions, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ModuleVersions;
  static deserializeBinaryFromReader(message: ModuleVersions, reader: jspb.BinaryReader): ModuleVersions;
}

export namespace ModuleVersions {
  export type AsObject = {
    versionsList: Array<ModuleVersion.AsObject>,
  }
}

export class HostRewrite extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getSource(): string;
  setSource(value: string): void;

  getDestination(): string;
  setDestination(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HostRewrite.AsObject;
  static toObject(includeInstance: boolean, msg: HostRewrite): HostRewrite.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: HostRewrite, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HostRewrite;
  static deserializeBinaryFromReader(message: HostRewrite, reader: jspb.BinaryReader): HostRewrite;
}

export namespace HostRewrite {
  export type AsObject = {
    id: string,
    source: string,
    destination: string,
  }
}

export class HostRewrites extends jspb.Message {
  clearRulesList(): void;
  getRulesList(): Array<HostRewrite>;
  setRulesList(value: Array<HostRewrite>): void;
  addRules(value?: HostRewrite, index?: number): HostRewrite;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HostRewrites.AsObject;
  static toObject(includeInstance: boolean, msg: HostRewrites): HostRewrites.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: HostRewrites, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HostRewrites;
  static deserializeBinaryFromReader(message: HostRewrites, reader: jspb.BinaryReader): HostRewrites;
}

export namespace HostRewrites {
  export type AsObject = {
    rulesList: Array<HostRewrite.AsObject>,
  }
}

