// package: v1
// file: domain/v1/remote.proto

import * as jspb from "google-protobuf";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as domain_v1_client_pb from "../../domain/v1/client_pb";

export class Remote extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getName(): string;
  setName(value: string): void;

  getUri(): string;
  setUri(value: string): void;

  hasEnabled(): boolean;
  clearEnabled(): void;
  getEnabled(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setEnabled(value?: google_protobuf_wrappers_pb.BoolValue): void;

  getArchetype(): string;
  setArchetype(value: string): void;

  clearAllowlistList(): void;
  getAllowlistList(): Array<string>;
  setAllowlistList(value: Array<string>): void;
  addAllowlist(value: string, index?: number): string;

  clearBlocklistList(): void;
  getBlocklistList(): Array<string>;
  setBlocklistList(value: Array<string>): void;
  addBlocklist(value: string, index?: number): string;

  clearRestrictedheadersList(): void;
  getRestrictedheadersList(): Array<string>;
  setRestrictedheadersList(value: Array<string>): void;
  addRestrictedheaders(value: string, index?: number): string;

  getStriprestricted(): boolean;
  setStriprestricted(value: boolean): void;

  hasClientprofile(): boolean;
  clearClientprofile(): void;
  getClientprofile(): domain_v1_client_pb.ClientProfile | undefined;
  setClientprofile(value?: domain_v1_client_pb.ClientProfile): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Remote.AsObject;
  static toObject(includeInstance: boolean, msg: Remote): Remote.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Remote, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Remote;
  static deserializeBinaryFromReader(message: Remote, reader: jspb.BinaryReader): Remote;
}

export namespace Remote {
  export type AsObject = {
    id: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    name: string,
    uri: string,
    enabled?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    archetype: string,
    allowlistList: Array<string>,
    blocklistList: Array<string>,
    restrictedheadersList: Array<string>,
    striprestricted: boolean,
    clientprofile?: domain_v1_client_pb.ClientProfile.AsObject,
  }
}

export class Remotes extends jspb.Message {
  clearRemotesList(): void;
  getRemotesList(): Array<Remote>;
  setRemotesList(value: Array<Remote>): void;
  addRemotes(value?: Remote, index?: number): Remote;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Remotes.AsObject;
  static toObject(includeInstance: boolean, msg: Remotes): Remotes.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Remotes, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Remotes;
  static deserializeBinaryFromReader(message: Remotes, reader: jspb.BinaryReader): Remotes;
}

export namespace Remotes {
  export type AsObject = {
    remotesList: Array<Remote.AsObject>,
  }
}

