// package: v1
// file: domain/v1/reactor.proto

import * as jspb from "google-protobuf";
import * as domain_v1_remote_pb from "../../domain/v1/remote_pb";
import * as domain_v1_refract_pb from "../../domain/v1/refract_pb";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";

export class GetContextRequest extends jspb.Message {
  getKey(): string;
  setKey(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetContextRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetContextRequest): GetContextRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetContextRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetContextRequest;
  static deserializeBinaryFromReader(message: GetContextRequest, reader: jspb.BinaryReader): GetContextRequest;
}

export namespace GetContextRequest {
  export type AsObject = {
    key: string,
  }
}

export class GetContextResponse extends jspb.Message {
  hasRefraction(): boolean;
  clearRefraction(): void;
  getRefraction(): domain_v1_refract_pb.Refraction | undefined;
  setRefraction(value?: domain_v1_refract_pb.Refraction): void;

  clearRemotesList(): void;
  getRemotesList(): Array<domain_v1_remote_pb.Remote>;
  setRemotesList(value: Array<domain_v1_remote_pb.Remote>): void;
  addRemotes(value?: domain_v1_remote_pb.Remote, index?: number): domain_v1_remote_pb.Remote;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetContextResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetContextResponse): GetContextResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetContextResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetContextResponse;
  static deserializeBinaryFromReader(message: GetContextResponse, reader: jspb.BinaryReader): GetContextResponse;
}

export namespace GetContextResponse {
  export type AsObject = {
    refraction?: domain_v1_refract_pb.Refraction.AsObject,
    remotesList: Array<domain_v1_remote_pb.Remote.AsObject>,
  }
}

export class GetConfigResponse extends jspb.Message {
  getHash(): string;
  setHash(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetConfigResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetConfigResponse): GetConfigResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetConfigResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetConfigResponse;
  static deserializeBinaryFromReader(message: GetConfigResponse, reader: jspb.BinaryReader): GetConfigResponse;
}

export namespace GetConfigResponse {
  export type AsObject = {
    hash: string,
  }
}

export class ReactorStatus extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getConfigHash(): string;
  setConfigHash(value: string): void;

  getTime(): number;
  setTime(value: number): void;

  getName(): string;
  setName(value: string): void;

  clearControlRodsList(): void;
  getControlRodsList(): Array<string>;
  setControlRodsList(value: Array<string>): void;
  addControlRods(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReactorStatus.AsObject;
  static toObject(includeInstance: boolean, msg: ReactorStatus): ReactorStatus.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReactorStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReactorStatus;
  static deserializeBinaryFromReader(message: ReactorStatus, reader: jspb.BinaryReader): ReactorStatus;
}

export namespace ReactorStatus {
  export type AsObject = {
    id: string,
    configHash: string,
    time: number,
    name: string,
    controlRodsList: Array<string>,
  }
}

export class ReactorStatuses extends jspb.Message {
  clearStatusesList(): void;
  getStatusesList(): Array<ReactorStatus>;
  setStatusesList(value: Array<ReactorStatus>): void;
  addStatuses(value?: ReactorStatus, index?: number): ReactorStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReactorStatuses.AsObject;
  static toObject(includeInstance: boolean, msg: ReactorStatuses): ReactorStatuses.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReactorStatuses, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReactorStatuses;
  static deserializeBinaryFromReader(message: ReactorStatuses, reader: jspb.BinaryReader): ReactorStatuses;
}

export namespace ReactorStatuses {
  export type AsObject = {
    statusesList: Array<ReactorStatus.AsObject>,
  }
}

