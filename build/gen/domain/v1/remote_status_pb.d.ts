// package: v1
// file: domain/v1/remote_status.proto

import * as jspb from "google-protobuf";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class RemoteStatus extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getStatus(): RemoteStatus.StatusMap[keyof RemoteStatus.StatusMap];
  setStatus(value: RemoteStatus.StatusMap[keyof RemoteStatus.StatusMap]): void;

  getTotal(): number;
  setTotal(value: number): void;

  getLatency(): number;
  setLatency(value: number): void;

  getRemoteid(): string;
  setRemoteid(value: string): void;

  getHealthy(): number;
  setHealthy(value: number): void;

  getUnhealthy(): number;
  setUnhealthy(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RemoteStatus.AsObject;
  static toObject(includeInstance: boolean, msg: RemoteStatus): RemoteStatus.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RemoteStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RemoteStatus;
  static deserializeBinaryFromReader(message: RemoteStatus, reader: jspb.BinaryReader): RemoteStatus;
}

export namespace RemoteStatus {
  export type AsObject = {
    id: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    status: RemoteStatus.StatusMap[keyof RemoteStatus.StatusMap],
    total: number,
    latency: number,
    remoteid: string,
    healthy: number,
    unhealthy: number,
  }

  export interface StatusMap {
    UNKNOWN: 0;
    OUT_OF_SERVICE: 1;
    IN_SERVICE: 2;
  }

  export const Status: StatusMap;
}

export class RemoteStatuses extends jspb.Message {
  clearStatusList(): void;
  getStatusList(): Array<RemoteStatus>;
  setStatusList(value: Array<RemoteStatus>): void;
  addStatus(value?: RemoteStatus, index?: number): RemoteStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RemoteStatuses.AsObject;
  static toObject(includeInstance: boolean, msg: RemoteStatuses): RemoteStatuses.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RemoteStatuses, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RemoteStatuses;
  static deserializeBinaryFromReader(message: RemoteStatuses, reader: jspb.BinaryReader): RemoteStatuses;
}

export namespace RemoteStatuses {
  export type AsObject = {
    statusList: Array<RemoteStatus.AsObject>,
  }
}

