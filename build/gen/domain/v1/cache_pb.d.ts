// package: v1
// file: domain/v1/cache.proto

import * as jspb from "google-protobuf";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class CacheEntry extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUri(): string;
  setUri(value: string): void;

  getDownloadcount(): number;
  setDownloadcount(value: number): void;

  getRemoteid(): string;
  setRemoteid(value: string): void;

  getRefractid(): string;
  setRefractid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CacheEntry.AsObject;
  static toObject(includeInstance: boolean, msg: CacheEntry): CacheEntry.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CacheEntry, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CacheEntry;
  static deserializeBinaryFromReader(message: CacheEntry, reader: jspb.BinaryReader): CacheEntry;
}

export namespace CacheEntry {
  export type AsObject = {
    id: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    uri: string,
    downloadcount: number,
    remoteid: string,
    refractid: string,
  }
}

export class CacheEntries extends jspb.Message {
  clearEntriesList(): void;
  getEntriesList(): Array<CacheEntry>;
  setEntriesList(value: Array<CacheEntry>): void;
  addEntries(value?: CacheEntry, index?: number): CacheEntry;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CacheEntries.AsObject;
  static toObject(includeInstance: boolean, msg: CacheEntries): CacheEntries.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CacheEntries, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CacheEntries;
  static deserializeBinaryFromReader(message: CacheEntries, reader: jspb.BinaryReader): CacheEntries;
}

export namespace CacheEntries {
  export type AsObject = {
    entriesList: Array<CacheEntry.AsObject>,
  }
}

export class CacheSlice extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getEntryid(): string;
  setEntryid(value: string): void;

  getUri(): string;
  setUri(value: string): void;

  getHash(): string;
  setHash(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CacheSlice.AsObject;
  static toObject(includeInstance: boolean, msg: CacheSlice): CacheSlice.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CacheSlice, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CacheSlice;
  static deserializeBinaryFromReader(message: CacheSlice, reader: jspb.BinaryReader): CacheSlice;
}

export namespace CacheSlice {
  export type AsObject = {
    id: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    entryid: string,
    uri: string,
    hash: string,
  }
}

export class CacheSlices extends jspb.Message {
  clearSlicesList(): void;
  getSlicesList(): Array<CacheSlice>;
  setSlicesList(value: Array<CacheSlice>): void;
  addSlices(value?: CacheSlice, index?: number): CacheSlice;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CacheSlices.AsObject;
  static toObject(includeInstance: boolean, msg: CacheSlices): CacheSlices.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CacheSlices, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CacheSlices;
  static deserializeBinaryFromReader(message: CacheSlices, reader: jspb.BinaryReader): CacheSlices;
}

export namespace CacheSlices {
  export type AsObject = {
    slicesList: Array<CacheSlice.AsObject>,
  }
}

