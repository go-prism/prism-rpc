// package: v1
// file: domain/v1/roles.proto

import * as jspb from "google-protobuf";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";

export class RoleBinding extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getUsername(): string;
  setUsername(value: string): void;

  getSubject(): string;
  setSubject(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleBinding.AsObject;
  static toObject(includeInstance: boolean, msg: RoleBinding): RoleBinding.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RoleBinding, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleBinding;
  static deserializeBinaryFromReader(message: RoleBinding, reader: jspb.BinaryReader): RoleBinding;
}

export namespace RoleBinding {
  export type AsObject = {
    id: string,
    name: string,
    username: string,
    subject: string,
  }
}

export class RoleBindings extends jspb.Message {
  clearRolesList(): void;
  getRolesList(): Array<RoleBinding>;
  setRolesList(value: Array<RoleBinding>): void;
  addRoles(value?: RoleBinding, index?: number): RoleBinding;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleBindings.AsObject;
  static toObject(includeInstance: boolean, msg: RoleBindings): RoleBindings.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RoleBindings, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleBindings;
  static deserializeBinaryFromReader(message: RoleBindings, reader: jspb.BinaryReader): RoleBindings;
}

export namespace RoleBindings {
  export type AsObject = {
    rolesList: Array<RoleBinding.AsObject>,
  }
}

