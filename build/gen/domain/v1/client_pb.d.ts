// package: v1
// file: domain/v1/client.proto

import * as jspb from "google-protobuf";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";

export class ClientProfile extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getSkiptlsverify(): boolean;
  setSkiptlsverify(value: boolean): void;

  getTlscert(): string;
  setTlscert(value: string): void;

  getTlskey(): string;
  setTlskey(value: string): void;

  getTlsca(): string;
  setTlsca(value: string): void;

  getCiphers(): ClientProfile.CipherProfileMap[keyof ClientProfile.CipherProfileMap];
  setCiphers(value: ClientProfile.CipherProfileMap[keyof ClientProfile.CipherProfileMap]): void;

  getTlsminversion(): number;
  setTlsminversion(value: number): void;

  getHttpproxy(): string;
  setHttpproxy(value: string): void;

  getHttpsproxy(): string;
  setHttpsproxy(value: string): void;

  getNoproxy(): string;
  setNoproxy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ClientProfile): ClientProfile.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientProfile;
  static deserializeBinaryFromReader(message: ClientProfile, reader: jspb.BinaryReader): ClientProfile;
}

export namespace ClientProfile {
  export type AsObject = {
    id: string,
    skiptlsverify: boolean,
    tlscert: string,
    tlskey: string,
    tlsca: string,
    ciphers: ClientProfile.CipherProfileMap[keyof ClientProfile.CipherProfileMap],
    tlsminversion: number,
    httpproxy: string,
    httpsproxy: string,
    noproxy: string,
  }

  export interface CipherProfileMap {
    MODERN: 0;
    INTERMEDIATE: 1;
    LEGACY: 2;
  }

  export const CipherProfile: CipherProfileMap;
}

