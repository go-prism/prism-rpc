// package: v1
// file: domain/v1/refract.proto

import * as jspb from "google-protobuf";
import * as github_com_infobloxopen_protoc_gen_gorm@v0_20_1_options_gorm_pb from "../../github.com/infobloxopen/protoc-gen-gorm@v0.20.1/options/gorm_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class Refraction extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getName(): string;
  setName(value: string): void;

  getArchetype(): string;
  setArchetype(value: string): void;

  clearRemotesList(): void;
  getRemotesList(): Array<string>;
  setRemotesList(value: Array<string>): void;
  addRemotes(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Refraction.AsObject;
  static toObject(includeInstance: boolean, msg: Refraction): Refraction.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Refraction, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Refraction;
  static deserializeBinaryFromReader(message: Refraction, reader: jspb.BinaryReader): Refraction;
}

export namespace Refraction {
  export type AsObject = {
    id: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    name: string,
    archetype: string,
    remotesList: Array<string>,
  }
}

export class Refractions extends jspb.Message {
  clearRefractionsList(): void;
  getRefractionsList(): Array<Refraction>;
  setRefractionsList(value: Array<Refraction>): void;
  addRefractions(value?: Refraction, index?: number): Refraction;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Refractions.AsObject;
  static toObject(includeInstance: boolean, msg: Refractions): Refractions.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Refractions, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Refractions;
  static deserializeBinaryFromReader(message: Refractions, reader: jspb.BinaryReader): Refractions;
}

export namespace Refractions {
  export type AsObject = {
    refractionsList: Array<Refraction.AsObject>,
  }
}

