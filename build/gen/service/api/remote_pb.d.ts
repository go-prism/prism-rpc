// package: api
// file: service/api/remote.proto

import * as jspb from "google-protobuf";
import * as domain_v1_remote_pb from "../../domain/v1/remote_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";

export class ListRequest extends jspb.Message {
  getArchetype(): string;
  setArchetype(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListRequest): ListRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListRequest;
  static deserializeBinaryFromReader(message: ListRequest, reader: jspb.BinaryReader): ListRequest;
}

export namespace ListRequest {
  export type AsObject = {
    archetype: string,
  }
}

export class GetRemoteRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRemoteRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetRemoteRequest): GetRemoteRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetRemoteRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRemoteRequest;
  static deserializeBinaryFromReader(message: GetRemoteRequest, reader: jspb.BinaryReader): GetRemoteRequest;
}

export namespace GetRemoteRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteRemoteRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteRemoteRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteRemoteRequest): DeleteRemoteRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteRemoteRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteRemoteRequest;
  static deserializeBinaryFromReader(message: DeleteRemoteRequest, reader: jspb.BinaryReader): DeleteRemoteRequest;
}

export namespace DeleteRemoteRequest {
  export type AsObject = {
    id: string,
  }
}

export class PatchRemoteRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasReq(): boolean;
  clearReq(): void;
  getReq(): CreateRemoteRequest | undefined;
  setReq(value?: CreateRemoteRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PatchRemoteRequest.AsObject;
  static toObject(includeInstance: boolean, msg: PatchRemoteRequest): PatchRemoteRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PatchRemoteRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PatchRemoteRequest;
  static deserializeBinaryFromReader(message: PatchRemoteRequest, reader: jspb.BinaryReader): PatchRemoteRequest;
}

export namespace PatchRemoteRequest {
  export type AsObject = {
    id: string,
    req?: CreateRemoteRequest.AsObject,
  }
}

export class CreateRemoteRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getUri(): string;
  setUri(value: string): void;

  getArchetype(): string;
  setArchetype(value: string): void;

  clearAllowlistList(): void;
  getAllowlistList(): Array<string>;
  setAllowlistList(value: Array<string>): void;
  addAllowlist(value: string, index?: number): string;

  clearBlocklistList(): void;
  getBlocklistList(): Array<string>;
  setBlocklistList(value: Array<string>): void;
  addBlocklist(value: string, index?: number): string;

  clearRestrictedheadersList(): void;
  getRestrictedheadersList(): Array<string>;
  setRestrictedheadersList(value: Array<string>): void;
  addRestrictedheaders(value: string, index?: number): string;

  getStriprestricted(): boolean;
  setStriprestricted(value: boolean): void;

  getEnabled(): boolean;
  setEnabled(value: boolean): void;

  hasClientprofile(): boolean;
  clearClientprofile(): void;
  getClientprofile(): UpdateProfileRequest | undefined;
  setClientprofile(value?: UpdateProfileRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateRemoteRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateRemoteRequest): CreateRemoteRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateRemoteRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateRemoteRequest;
  static deserializeBinaryFromReader(message: CreateRemoteRequest, reader: jspb.BinaryReader): CreateRemoteRequest;
}

export namespace CreateRemoteRequest {
  export type AsObject = {
    name: string,
    uri: string,
    archetype: string,
    allowlistList: Array<string>,
    blocklistList: Array<string>,
    restrictedheadersList: Array<string>,
    striprestricted: boolean,
    enabled: boolean,
    clientprofile?: UpdateProfileRequest.AsObject,
  }
}

export class UpdateProfileRequest extends jspb.Message {
  getCa(): string;
  setCa(value: string): void;

  getCert(): string;
  setCert(value: string): void;

  getKey(): string;
  setKey(value: string): void;

  getSkiptls(): boolean;
  setSkiptls(value: boolean): void;

  getHttpproxy(): string;
  setHttpproxy(value: string): void;

  getHttpsproxy(): string;
  setHttpsproxy(value: string): void;

  getNoproxy(): string;
  setNoproxy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateProfileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateProfileRequest): UpdateProfileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateProfileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateProfileRequest;
  static deserializeBinaryFromReader(message: UpdateProfileRequest, reader: jspb.BinaryReader): UpdateProfileRequest;
}

export namespace UpdateProfileRequest {
  export type AsObject = {
    ca: string,
    cert: string,
    key: string,
    skiptls: boolean,
    httpproxy: string,
    httpsproxy: string,
    noproxy: string,
  }
}

