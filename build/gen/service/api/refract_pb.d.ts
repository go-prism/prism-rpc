// package: api
// file: service/api/refract.proto

import * as jspb from "google-protobuf";
import * as domain_v1_refract_pb from "../../domain/v1/refract_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";

export class GetRefractRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRefractRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetRefractRequest): GetRefractRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetRefractRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRefractRequest;
  static deserializeBinaryFromReader(message: GetRefractRequest, reader: jspb.BinaryReader): GetRefractRequest;
}

export namespace GetRefractRequest {
  export type AsObject = {
    id: string,
  }
}

export class CreateRefractRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getArchetype(): string;
  setArchetype(value: string): void;

  clearRemotesList(): void;
  getRemotesList(): Array<string>;
  setRemotesList(value: Array<string>): void;
  addRemotes(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateRefractRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateRefractRequest): CreateRefractRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateRefractRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateRefractRequest;
  static deserializeBinaryFromReader(message: CreateRefractRequest, reader: jspb.BinaryReader): CreateRefractRequest;
}

export namespace CreateRefractRequest {
  export type AsObject = {
    name: string,
    archetype: string,
    remotesList: Array<string>,
  }
}

export class PatchRefractRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasReq(): boolean;
  clearReq(): void;
  getReq(): CreateRefractRequest | undefined;
  setReq(value?: CreateRefractRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PatchRefractRequest.AsObject;
  static toObject(includeInstance: boolean, msg: PatchRefractRequest): PatchRefractRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PatchRefractRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PatchRefractRequest;
  static deserializeBinaryFromReader(message: PatchRefractRequest, reader: jspb.BinaryReader): PatchRefractRequest;
}

export namespace PatchRefractRequest {
  export type AsObject = {
    id: string,
    req?: CreateRefractRequest.AsObject,
  }
}

export class DeleteRefractRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteRefractRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteRefractRequest): DeleteRefractRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteRefractRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteRefractRequest;
  static deserializeBinaryFromReader(message: DeleteRefractRequest, reader: jspb.BinaryReader): DeleteRefractRequest;
}

export namespace DeleteRefractRequest {
  export type AsObject = {
    id: string,
  }
}

