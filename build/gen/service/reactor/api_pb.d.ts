// package: reactor
// file: service/reactor/api.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";

export class InvalidateControlRodRequest extends jspb.Message {
  getRefraction(): string;
  setRefraction(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InvalidateControlRodRequest.AsObject;
  static toObject(includeInstance: boolean, msg: InvalidateControlRodRequest): InvalidateControlRodRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InvalidateControlRodRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InvalidateControlRodRequest;
  static deserializeBinaryFromReader(message: InvalidateControlRodRequest, reader: jspb.BinaryReader): InvalidateControlRodRequest;
}

export namespace InvalidateControlRodRequest {
  export type AsObject = {
    refraction: string,
  }
}

export class PutObjectV2Request extends jspb.Message {
  hasContent(): boolean;
  clearContent(): void;
  getContent(): Uint8Array | string;
  getContent_asU8(): Uint8Array;
  getContent_asB64(): string;
  setContent(value: Uint8Array | string): void;

  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): GetObjectRequest | undefined;
  setRequest(value?: GetObjectRequest): void;

  getMsgCase(): PutObjectV2Request.MsgCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PutObjectV2Request.AsObject;
  static toObject(includeInstance: boolean, msg: PutObjectV2Request): PutObjectV2Request.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PutObjectV2Request, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PutObjectV2Request;
  static deserializeBinaryFromReader(message: PutObjectV2Request, reader: jspb.BinaryReader): PutObjectV2Request;
}

export namespace PutObjectV2Request {
  export type AsObject = {
    content: Uint8Array | string,
    request?: GetObjectRequest.AsObject,
  }

  export enum MsgCase {
    MSG_NOT_SET = 0,
    CONTENT = 1,
    REQUEST = 2,
  }
}

export class ListObjectRequest extends jspb.Message {
  getRefraction(): string;
  setRefraction(value: string): void;

  getPrefix(): string;
  setPrefix(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListObjectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListObjectRequest): ListObjectRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListObjectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListObjectRequest;
  static deserializeBinaryFromReader(message: ListObjectRequest, reader: jspb.BinaryReader): ListObjectRequest;
}

export namespace ListObjectRequest {
  export type AsObject = {
    refraction: string,
    prefix: string,
  }
}

export class ListObjectResponse extends jspb.Message {
  clearObjectsList(): void;
  getObjectsList(): Array<string>;
  setObjectsList(value: Array<string>): void;
  addObjects(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListObjectResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListObjectResponse): ListObjectResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListObjectResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListObjectResponse;
  static deserializeBinaryFromReader(message: ListObjectResponse, reader: jspb.BinaryReader): ListObjectResponse;
}

export namespace ListObjectResponse {
  export type AsObject = {
    objectsList: Array<string>,
  }
}

export class GetObjectRequest extends jspb.Message {
  getRefraction(): string;
  setRefraction(value: string): void;

  getPath(): string;
  setPath(value: string): void;

  getMethod(): string;
  setMethod(value: string): void;

  getHeadersMap(): jspb.Map<string, string>;
  clearHeadersMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetObjectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetObjectRequest): GetObjectRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetObjectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetObjectRequest;
  static deserializeBinaryFromReader(message: GetObjectRequest, reader: jspb.BinaryReader): GetObjectRequest;
}

export namespace GetObjectRequest {
  export type AsObject = {
    refraction: string,
    path: string,
    method: string,
    headersMap: Array<[string, string]>,
  }
}

export class GetObjectResponse extends jspb.Message {
  getCode(): number;
  setCode(value: number): void;

  getMessage(): string;
  setMessage(value: string): void;

  getLocation(): string;
  setLocation(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetObjectResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetObjectResponse): GetObjectResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetObjectResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetObjectResponse;
  static deserializeBinaryFromReader(message: GetObjectResponse, reader: jspb.BinaryReader): GetObjectResponse;
}

export namespace GetObjectResponse {
  export type AsObject = {
    code: number,
    message: string,
    location: string,
  }
}

export class GetObjectV2Response extends jspb.Message {
  getContent(): Uint8Array | string;
  getContent_asU8(): Uint8Array;
  getContent_asB64(): string;
  setContent(value: Uint8Array | string): void;

  getCode(): number;
  setCode(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetObjectV2Response.AsObject;
  static toObject(includeInstance: boolean, msg: GetObjectV2Response): GetObjectV2Response.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetObjectV2Response, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetObjectV2Response;
  static deserializeBinaryFromReader(message: GetObjectV2Response, reader: jspb.BinaryReader): GetObjectV2Response;
}

export namespace GetObjectV2Response {
  export type AsObject = {
    content: Uint8Array | string,
    code: number,
  }
}

export class HasObjectResponse extends jspb.Message {
  getOk(): boolean;
  setOk(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HasObjectResponse.AsObject;
  static toObject(includeInstance: boolean, msg: HasObjectResponse): HasObjectResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: HasObjectResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HasObjectResponse;
  static deserializeBinaryFromReader(message: HasObjectResponse, reader: jspb.BinaryReader): HasObjectResponse;
}

export namespace HasObjectResponse {
  export type AsObject = {
    ok: boolean,
  }
}

export class GetRefractInfoRequest extends jspb.Message {
  getRefraction(): string;
  setRefraction(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRefractInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetRefractInfoRequest): GetRefractInfoRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetRefractInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRefractInfoRequest;
  static deserializeBinaryFromReader(message: GetRefractInfoRequest, reader: jspb.BinaryReader): GetRefractInfoRequest;
}

export namespace GetRefractInfoRequest {
  export type AsObject = {
    refraction: string,
  }
}

export class GetRefractInfoResponse extends jspb.Message {
  getSize(): number;
  setSize(value: number): void;

  getFiles(): number;
  setFiles(value: number): void;

  getMetadataMap(): jspb.Map<string, string>;
  clearMetadataMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRefractInfoResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetRefractInfoResponse): GetRefractInfoResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetRefractInfoResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRefractInfoResponse;
  static deserializeBinaryFromReader(message: GetRefractInfoResponse, reader: jspb.BinaryReader): GetRefractInfoResponse;
}

export namespace GetRefractInfoResponse {
  export type AsObject = {
    size: number,
    files: number,
    metadataMap: Array<[string, string]>,
  }
}

export class ReactorStatus extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  clearControlrodsList(): void;
  getControlrodsList(): Array<string>;
  setControlrodsList(value: Array<string>): void;
  addControlrods(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReactorStatus.AsObject;
  static toObject(includeInstance: boolean, msg: ReactorStatus): ReactorStatus.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReactorStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReactorStatus;
  static deserializeBinaryFromReader(message: ReactorStatus, reader: jspb.BinaryReader): ReactorStatus;
}

export namespace ReactorStatus {
  export type AsObject = {
    id: string,
    controlrodsList: Array<string>,
  }
}

